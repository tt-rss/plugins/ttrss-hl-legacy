<?php
class HL_Legacy extends Plugin {
	function about() {
		return array(1.0,
			"Revert back to legacy headline click behavior",
			"fox");
	}

	function init($host) {

	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}
}
