require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	ready(function() {
		Headlines.click = function (event, id, in_body) {
			in_body = in_body || false;

			if (App.isCombinedMode()) {

				if (!in_body && (event.ctrlKey || id == Article.getActive() || App.getInitParam("cdm_expanded"))) {
					Article.openInNewWindow(id);
					Headlines.toggleUnread(id, 0);
					return false;
				}

				if (Article.getActive() != id) {
					Article.setActive(id);

					if (!App.getInitParam("cdm_expanded"))
						Article.cdmMoveToId(id);
				} else if (in_body) {
					Headlines.toggleUnread(id, 0);
				}

				return in_body;

			} else {
				if (event.ctrlKey) {
					Article.openInNewWindow(id);
					Headlines.toggleUnread(id, 0);
				} else {
					Article.view(id);
				}

				return false;
			}
		}
	});
});
